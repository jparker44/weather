import axios from 'axios';

const API_KEY = '987b480635be54595fa07564ff0f3109';
const ROOT_URL = `https://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city) {
    const countryCode = "nl";
    const url = `${ROOT_URL}&q=${city},${countryCode}`;
    const request = axios.get(url);

    return {
        type: FETCH_WEATHER,
        payload: request
    };
}
